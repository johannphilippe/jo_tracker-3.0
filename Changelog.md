# Version 3.0

## v 3.0.0

### Features

- Parameters Dialogs (audio and general)
- Macro/Global Csound code 
- Handwritten GEN 
- Scriptable curves
- Multichannel waveform
- New timer on main loop (with registered callbacks for audio callback display)
- Oscilloscope (experimental)
- Csound code editor (UDO and instruments)
- Spline and Bezier mode in curve editor
- Sequence arranger (left expander in Tracks tab)
- Output modes : export CSD, export GENS, render, render instances (different soundfiles for every instance of every instruments)
- Load orchestra from project (experimental)
- Play modes (play from here, play seq, play from start, and play recording)
- Lua API allowing to script score data
- Auto calculation for duration p-field (by instance, by occurence, or disabled)
- Internal data storage (in a home folder). Data are stored internally every time user changes SEQNBR, or presses Ctrl+s.
- Save project or export CSD with gathering (soundfiles, scripted samples)

## v 3.0.1

### Features

- FFT Lua API (static)
- Math Utils Lua API
- About menu (contains informations and documentation)

### Fixed

- Resize issue in main tab when loading / create new project
- Rendering error due to an uninitialized variable in parameters data (additional options).
- Syntax highlighting in Scriptable Curve editor.
- Performance in multichannel waveform. Now each waveform is defined on 4096 samples.
- Fixed crash on waveform plot when loading a soundfile with less channels than previous (plot list wasn't properly resized)

## v 3.0.2

### Features 

- Solo and Mute modes in tracks
- Expand or completely hide a track
- Oscilloscope is now fully working (with Sample size selector)
- Detachable Gen Editor

### Fixed

- Multichannel Waveform plot (crash issue)
- Resize dialog issue
 
### To do 

- Improve and fix FFT API in Lua
- Editable waveform
- Spectral visualisation in waveforms (spectral edit)
- a Csound code editor to edit ? (would allow many manipulations, without many buttons)
- Lua API : add interval API for data manipulation
- Undo/Redo in Matrices - requires to modify the filling mechanisms. Or implement a non-native one (like only the meaningful operations).
