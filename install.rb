#!/usr/bin/ruby


#    Copyright (C) 2019-2020 Johann Philippe

#    This file is part of jo_tracker

#    The jo_tracker software is free software; you can redistribute it
#    and/or modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.

#    jo_tracker is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public
#    License along with jo_tracker; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#    02110-1301 USA

#    As a special exception, if other files instantiate templates or
#    use macros or inline functions from this file, this file does not
#    by itself cause the resulting executable or library to be covered
#    by the GNU Lesser General Public License. This exception does not
#    however invalidate any other reasons why the library or executable
#    file might be covered by the GNU Lesser General Public License.




puts %x(gem install bundler)

require "bundler/inline"
require "rubygems/package"

puts "Downloading Gem's dependancies"

gemfile do
	source 'https://rubygems.org'
	gem "down", require: true
	gem "fileutils", require: true
	gem 'archive-zip', require: false
	gem "zlib", require: true
end
	puts "Gem downloaded"

	require "archive/zip"




def unzip(file, destination)
	print "Start unzip ", file , "\n"
	FileUtils.mkdir_p(destination)
=begin
	Zip::ZipFile.open(file) do |zip_file|
		zip_file.each do |f|
			fpath = File.join(destination, f.name)
			zip_file.extract(f,fpath) unless File.exist?(fpath)
			end
	end
=end

	Archive::Zip.extract(file, destination)
	FileUtils.rm(file)
end


def untar(file, destination)
	FileUtils.mkdir_p(destination)
	tar_extract = Gem::Package::TarReader.new(Zlib::GzipReader.open(file))
	tar_extract.rewind
	tar_extract.each do |entry|

		if entry.directory?
			FileUtils.mkdir_p(destination + "/" + entry.full_name)	
		end

		if entry.file?
			if entry.full_name =~ /\// # check if contains a slash and is then a folder
				folder_name = entry.full_name[0 ... entry.full_name.rindex(/\//)]
				FileUtils.mkdir_p(destination + "/" + folder_name)
			end

			f = File.new(destination + "/" + entry.full_name, "wb")
			f.write(entry.read)
			f.close
		end
	end
	tar_extract.close
	FileUtils.rm(file)
end


def download(path, tofile, extension)
	print "Start downloading " , tofile, "\n"
	tempfile = Down.download(path)
	print "Write " , tofile , " to filesystem \n"
	f = File.open(tofile + extension, "wb")
	f.write(tempfile.read)
	f.close
	if extension == ".zip"  
		unzip(tofile + extension, tofile)
	elsif extension == ".tar.gz" 
		untar(tofile + extension, tofile)
	end

	puts "End of download"

end


def install_linux
	puts "Starting installation for Linux"
	puts "Installing files"
	jo_tracker_path = Dir.home + "/.local/share/jo_tracker"
	FileUtils.mkdir_p jo_tracker_path
	FileUtils.mkdir_p(jo_tracker_path + "/terra")
	FileUtils.mkdir_p(jo_tracker_path + "/doc/html")
	FileUtils.mkdir_p(jo_tracker_path + "/doc/jo_tracker_documentation")
	FileUtils.mkdir_p(jo_tracker_path + "/examples")
	FileUtils.mkdir_p(jo_tracker_path + "/res")
	FileUtils.mkdir_p("/usr/share/icons/jo_tracker")

        FileUtils.cp_r Dir.pwd + "/doc/jo_tracker_documentation", jo_tracker_path + "/doc/jo_tracker_documentation"

	FileUtils.cp Dir.pwd + "/bin/jo_tracker", "/usr/bin"
	FileUtils.cp Dir.pwd + "/terra/json.lua", jo_tracker_path + "/terra"
	FileUtils.cp Dir.pwd + "/terra/jo_tracker_scripting.lua", jo_tracker_path + "/terra"
	FileUtils.cp Dir.pwd + "/doc/lua_api.html" , jo_tracker_path + "/doc"

	FileUtils.cp_r Dir.pwd + "/doc/html", jo_tracker_path + "/doc"

	FileUtils.cp Dir.pwd + "/res/udo.json" , jo_tracker_path + "/res"

	FileUtils.cp_r Dir.pwd + "/examples/.", jo_tracker_path + "/examples"

	FileUtils.cp Dir.pwd + "/icons/jo_tracker_icon.png", "/usr/share/icons/jo_tracker/"

	puts "Installation of dependencies"
	puts "Downloading luajit development kit"

	puts %x(apt install libluajit-5.1-dev)

	
	puts ("Downloading sndfile dev kit")
	puts %x(apt install -y libsndfile1) 
	puts %x(apt install -y libsndfile1-dev)
	# installing webkit -- trying to install all
	# find which was successfull

	puts "Downloading webkit"
	webkit_search = `echo | apt search libwebkit`

	if (webkit_search =~ /libwebkitgtk-3.0/)  or (webkit_search =~ /libwebkitgtk-1.0-0/) then
		wk_1_0 = system("apt install -y libwebkitgtk-1.0-0")
		wk_3_0 = system("apt install -y libwebkitgtk-3.0")
		#create symlink
		if(wk_1_0 == true) then 
			puts %x(ln -s /usr/lib/x86_64-linux-gnu/libwebkit-1.0.so /usr/lib/x86_64-linux-gnu/libwebkit.so)
		elsif(wk_3_0 == true) then 
			puts %x(ln -s /usr/lib/x86_64-linux-gnu/libwebkit-1.0.so /usr/lib/x86_64-linux-gnu/libwebkit.so)
		else
			puts "Didn't find any webkit to link with"
		end

	elsif webkit_search =~ /libwebkit2gtk-4.0-dev/ then
		puts %x(apt install -y libwebkit2gtk-4.0-dev)
		#create symlink
		puts %x(ln -s /usr/lib/x86_64-linux-gnu/libwebkit2gtk-4.0.so /usr/lib/x86_64-linux-gnu/libwebkit.so)

	else
			puts "Didn't find any webkit to install"
	end


	puts "Installation of Csound"
	puts "This will install last version of Csound. It might cause a problem if you are not using last version, and need and older one for any reason"
	puts "Do you agree ? [y,n]"
	val = gets
	val = val.downcase.strip
	if val=="y" or val=="yes" 
		puts "Installing Csound"
		download("https://github.com/csound/csound/releases/download/6.13.0/Csound6.13.0-Linux-x86_64.tar.gz", "csound", ".tar.gz")
		FileUtils.cp_r "csound/Csound6.13.0-Linux-x86_64/bin/.", "/usr/local/bin"
		FileUtils.cp_r "csound/Csound6.13.0-Linux-x86_64/include/.", "/usr/local/include"
		FileUtils.cp_r "csound/Csound6.13.0-Linux-x86_64/lib/.", "/usr/local/lib"

		FileUtils.rm_r("csound")


	end




	puts "Installation of IUP, CD and IM"
	download("https://sourceforge.net/projects/iup/files/3.30/Linux%20Libraries/iup-3.30_Linux415_64_lib.tar.gz/download", "iup",".tar.gz")
	download("https://sourceforge.net/projects/canvasdraw/files/5.14/Linux%20Libraries/cd-5.14_Linux415_64_lib.tar.gz/download","cd",".tar.gz")
	download("https://sourceforge.net/projects/imtoolkit/files/3.15/Linux%20Libraries/im-3.15_Linux415_64_lib.tar.gz/download","im",".tar.gz")

	puts %x(chmod u+x iup/install)
	puts %x(chmod u+x cd/install)
	puts %x(chmod u+x im/install)

	# Install FTGL
	FileUtils.cp( Dir.pwd + "/iup/ftgl/lib/Linux415_64/libftgl.so", "/usr/lib")

	#Finally no need of static libraries
	#puts %x(chmod u+x iup/install_dev)
	#puts %x(chmod u+x cd/install_dev)
	#puts %x(chmod u+x im/install_dev)


	install_cmd = "cd #{Dir.pwd}/iup && sudo ./install && sudo ./install_dev && cd .."
	system(install_cmd)
	install_cmd = "cd #{Dir.pwd}/cd && sudo ./install && sudo ./install_dev && cd .."
	system(install_cmd)
	install_cmd = "cd #{Dir.pwd}/im && sudo ./install && sudo ./install_dev && cd .."
	system(install_cmd)

	# Check if system uses lib64, if so create symlink from /usr/lib to /usr/lib64
	if Dir.exist?("/usr/lib64") then
		Dir.new("iup").each do |f|
			if f =~ /\.so/ then
				# Create symlink, ok copy in /usr/lib as well
				ln_cmd = "sudo ln -s /usr/lib64/#{f} /usr/lib/#{f}"
				system(ln_cmd)
			end
		end
		Dir.new("cd").each do |f|
			if f =~ /\.so/ then
				# Create symlink, ok copy in /usr/lib as well
				ln_cmd = "sudo ln -s /usr/lib64/#{f} /usr/lib/#{f}"
				system(ln_cmd)
			end
		end
		Dir.new("im").each do |f|
			if f =~ /\.so/ then
				# Create symlink, ok copy in /usr/lib as well
				ln_cmd = "sudo ln -s /usr/lib64/#{f} /usr/lib/#{f}"
				system(ln_cmd)
			end
		end
	end

	FileUtils.rm_r("iup")
	FileUtils.rm_r("cd")
	FileUtils.rm_r("im")



	# Change ownership in .local jo_tracker
	uname = ENV["USER"]
	cmd_ch = "sudo chown -R #{uname} ~/.local/share/jo_tracker"
	system cmd_ch
	cmd_ch = "sudo chmod +rwx ~/.local/share/jo_tracker"
	system cmd_ch

	puts "Installation of .desktop file"
	puts %x(sudo desktop-file-install desktop/jo_tracker.desktop)
	puts %x(sudo update-desktop-database /usr/share/applications)

	puts %x(sudo ldconfig)

	puts "jo_tracker installed successfully"
end








def install_windows
	puts "Starting installation for Windows"
	puts "Installing files"

	puts "Create home directories"
	jo_tracker_path = Dir.home + "/AppData/Local/jo_tracker"

	FileUtils.mkdir_p jo_tracker_path
	FileUtils.mkdir_p(jo_tracker_path + "/terra")
	FileUtils.mkdir_p(jo_tracker_path + "/doc/html")
	FileUtils.mkdir_p(jo_tracker_path + "/examples")
	FileUtils.mkdir_p(jo_tracker_path + "/res")

	puts "Copying files"
	FileUtils.mkdir_p(jo_tracker_path + "/doc/jo_tracker_documentation")
        FileUtils.cp_r Dir.pwd + "/doc/jo_tracker_documentation", jo_tracker_path + "/doc/jo_tracker_documentation"

        binary_path = "C:/Program Files/jo_tracker/bin"
        FileUtils.mkdir_p binary_path

        FileUtils.cp Dir.pwd + "/bin/jo_tracker.exe", binary_path
	FileUtils.cp Dir.pwd + "/terra/json.lua", jo_tracker_path + "/terra"
	FileUtils.cp Dir.pwd + "/terra/jo_tracker_scripting.lua", jo_tracker_path + "/terra"
	FileUtils.cp Dir.pwd + "/doc/lua_api.html" , jo_tracker_path + "/doc"

	FileUtils.cp_r Dir.pwd + "/doc/html", jo_tracker_path + "/doc/"
	FileUtils.cp Dir.pwd + "/res/udo.json" , jo_tracker_path + "/res"

	FileUtils.cp_r Dir.pwd + "/examples/.", jo_tracker_path + "/examples"

	# Not necessary since libterra is statically linked
	download("https://github.com/terralang/terra/releases/download/release-1.0.0-beta2/terra-Windows-x86_64-1c8dd1b.zip", "terra_lib", ".zip")

	install_path = "C:/Program Files/jo_tracker"
	bin_path = "#{install_path}/bin/"
	include_path = "#{install_path}/include/"
	FileUtils.mkdir_p bin_path
	FileUtils.mkdir_p include_path

	FileUtils.cp Dir.pwd + "/terra_lib/terra-Windows-x86_64-1c8dd1b/bin/terra.dll", "#{bin_path}terra.dll"
	FileUtils.cp Dir.pwd + "/terra_lib/terra-Windows-x86_64-1c8dd1b/bin/lua51.dll", "#{bin_path}lua51.dll"
	FileUtils.rm_r("terra_lib")

	puts "Installation of libsndfile"
			
	download("http://www.mega-nerd.com/libsndfile/files/1.0.29pre2/libsndfile-1.0.29pre2-w64.zip", "libsndfile", ".zip")
	#FileUtils.mkdir_p "C:/Program Files/libsndfile"

	#FileUtils.cp_r "libsndfile/.", "C:/Program Files/libsndfile"
	FileUtils.cp "libsndfile/bin/libsndfile-1.dll", "#{bin_path}"
	FileUtils.cp "libsndfile/include/sndfile.h", "#{include_path}"
	FileUtils.cp "libsndfile/include/sndfile.hh", "#{include_path}"

	FileUtils.rm_r "libsndfile"


	puts "Installation of Csound, select an option"
	puts "[1] Install in jo_tracker directory"
	puts "[2] Install in Csound Standard path \"C:/Program Files/Csound6_x64\""
	puts "[3] Do not install Csound, it is already on the system"
	val = gets
	val = val.strip
	if val == "1" or val =="2" then
			download("https://github.com/csound/csound/releases/download/6.14.0/Csound6.14.0-Windows_x64-binaries.zip", "csound", ".zip")
	end

	if val=="1" 
		#FileUtils.cp "csound/csound64.dll", bin_path
		#FileUtils.cp "csound/csnd6.dll", bin_path
		Dir.foreach("csound/") do |csfile|
			if csfile =~ /\.dll/ then
				FileUtils.cp Dir.pwd + "/csound/#{csdile}", bin_path
			end
		end

		FileUtils.cp_r "csound/include/.", include_path
		FileUtils.rm_r("csound")
		puts "Csound installed successfully"
	elsif val=="2" 
		download("https://github.com/csound/csound/releases/download/6.14.0/Csound6.14.0-Windows_x64-binaries.zip", "csound", ".zip")
		FileUtils.mkdir_p "C:/Program Files/Csound6_x64/lib"
		FileUtils.cp_r Dir.pwd + "/csound/.", "C:/Program Files/Csound6_x64"
		FileUtils.rm_r("csound")
		puts "Csound installed successfully"
	elsif val=="3"
		puts "Not installing Csound, assuming it is already there"
	end

	puts "Installation of IUP, CD and IM"


	#FileUtils.mkdir_p "C:/Program Files/IUP/iup"
	#FileUtils.mkdir_p "C:/Program Files/IUP/cd"
	#FileUtils.mkdir_p "C:/Program Files/IUP/im"

	iup_fpath = "lib/iup-3.30_Win64_dll15_lib"
	cd_fpath = "lib/cd-5.14_Win64_dll15_lib"
	im_fpath = "lib/im-3.15_Win64_dll15_lib"

	Dir.foreach("lib/iup-3.30_Win64_dll15_lib/.") do |iupfile|
		if iupfile =~ /\.dll/ then
			FileUtils.cp Dir.pwd + "/#{iup_fpath}/" + iupfile, bin_path
		end
	end
	Dir.foreach("lib/iup-3.30_Win64_dll15_lib/include/.") do |iupfile|
		if(File.directory?(Dir.pwd + "/#{iup_fpath}/include/#{iupfile}")== true) then next end
		FileUtils.cp Dir.pwd + "/#{iup_fpath}/include/" + iupfile, include_path
	end


	Dir.foreach("lib/cd-5.14_Win64_dll15_lib/.") do |cdfile|
		if cdfile =~ /\.dll/ then
			FileUtils.cp Dir.pwd + "/#{cd_fpath}/" + cdfile, bin_path
		end
	end
	Dir.foreach("lib/cd-5.14_Win64_dll15_lib/include/.") do |cdfile|
		if(File.directory?(Dir.pwd + "/#{cd_fpath}/include/#{cdfile}")== true) then next end
		FileUtils.cp Dir.pwd + "/#{cd_fpath}/include/#{cdfile}", include_path
	end

	Dir.foreach("lib/im-3.15_Win64_dll15_lib/.") do |imfile|
		if imfile =~ /\.dll/ then
			FileUtils.cp Dir.pwd + "/#{im_fpath}/#{imfile}", bin_path
		end
	end
	Dir.foreach("lib/im-3.15_Win64_dll15_lib/include/.") do |imfile|
		if(File.directory?(Dir.pwd + "/#{im_fpath}/include/#{imfile}")== true) then next end
		FileUtils.cp Dir.pwd + "/#{im_fpath}/include/#{imfile}", include_path
	end

	
#	puts %x(setx PATH "$env:Path;C:/Program Files/IUP" -m)

	puts "Configuration of environment variables"

	envpath  = ENV["Path"] + ";C:/Program Files/jo_tracker/bin;C:/Program Files/Csound6_x64;C:/Program Files/jo_tracker/lib"
#	envpath  = ENV["Path"] + ";C:/Program Files/jo_tracker/bin;C:/Program Files/IUP/iup;C:/Program Files/IUP/im;C:/Program Files/IUP/cd;C:/Program Files/Terra;C:/Program Files/Csound6_x64;C:/Program Files/libsndfile/bin"
	cmd = "powershell setx PATH '" + envpath + "' -m"
	system(cmd)

        puts "jo_tracker needs windows runtime library vc_runtime140_1.dll and the execution of VC_redist"
        puts "If you're not sure, you can still re-run this script if those dependencies are missing"
        puts "Do you want to install windows runtime ? [y,n]"
        v = gets
        val = v.downcase.strip
        if val == "y" or val == "yes" then
          #install vcruntime140_1.dll
          FileUtils.cp "win/vcruntime140_1.dll", "C:/Program Files/jo_tracker/bin"
          # install vcredis 2017
          res = system("./win/VC_redist.x64.exe")
        end


	puts "jo_tracker installed successfully"
end

	
print "Installation of jo_tracker, are you sure ? [y,n] : "
v = gets
val = v.downcase.strip	

if val == "y" or val =="yes" 
	if (RUBY_PLATFORM =~ /win32/) or (RUBY_PLATFORM =~ /mingw/)
		puts "jo_tracker for Windows"
		install_windows
	elsif RUBY_PLATFORM =~ /linux/
		puts "jo_tracker for Linux"
		install_linux
	else
		puts "jo_tracker is not available on this platform, please install jo_tracker on a recent Linux or Windows system."
	end
else
	puts "Installation canceled"
end


