
```
    |           /                   /
      ___      (___  ___  ___  ___ (     ___  ___ 
    )|   )     |    |   )|   )|    |___)|___)|   )
   / |__/      |__  |    |__/||__  | \  |__  |    
__/        ---
```
      
# JO_TRACKER 3.0
Beta version. 
If you encounter a bug or any issue with the software, feel free to report it by email, or to report an issue [here](https://framagit.org/johannphilippe/jo_tracker-3.0).

**Developped by [Johann Philippe](https://framagit.org/johannphilippe) in Lyon, France.**
**To contact me :** [johannphilippe@lilo.org](johannphilippe@lilo.org)



# License 
    Copyright (C) 2019-2020 Johann Philippe

    The jo_tracker software is free software; you can redistribute it
    and/or modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    jo_tracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with jo_tracker; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA

    As a special exception, if other files instantiate templates or
    use macros or inline functions from this file, this file does not
    by itself cause the resulting executable or library to be covered
    by the GNU Lesser General Public License. This exception does not
    however invalidate any other reasons why the library or executable
    file might be covered by the GNU Lesser General Public License.

