# Lua API
jo_tracker provides a [Terra](http://terralang.org/) embeded interpretor for combined Terra/Lua code, and a Lua API, allowing user to script tracks data. 
Most of those functions are used to get and set values in user data, with its indexes. Be careful that, if most indexes start at 1, tracks must be indexed starting at 0.
This small API is written in pure Lua, and can be modified/extended by modifying "jo_tracker_scripting.lua" file. This file is supposed to be located at "/usr/share/jo_tracker/terra".

## Jo_tracker API

### Setters 

`void jt.setNumlin(integer sequence, integer, line_nbr)`       
Sets the number of lines for selected sequence. Can also be called with `jt.setLignbr`.

`void jt.setNumcol(integer sequence, integer track, integer column_nbr)`        
Sets the number of lines for selected track, in selected sequence. Can also be called with `jt.setColnbr`.

`void jt.setTracknbr(integer track_nbr)`        
Sets the number of tracks

`void jt.setTempoAt(integer sequence, integer line, integer column, string value)`        
Sets the tempo (value) at the given indexes (sequence, line, column). If value is not a string, it will be converted to a string anyway.  

`void jt.setAt(integer sequence, integer track, integer line, integer column, string value)`        
Sets the value in selected sequence/track at selected line/column. If value passed is not a string, it will be converted to a string anyway. 

`void jt.setColumn(integer sequence, integer track, integer col, table values)`        
Fills the selected column in selected sequence with table values, only for lines where column 1 is filled with an instrument name. Performs lignbr and colnbr checking.   

`void jt.setLine(integer sequence, integer track, integer line, table line_value)`       
Fills the selected line with values contained in a Lua table. Returns false if number of lines or columns cannot be found, else true. 

`void jt.setValueInColumn(integer sequence, integer track, integer column, string value)`        
Fills the selected column with value. Only for lines wher column 1 is filledd with an instrument name. Performs colnbr checking

`void jt.copyLineTo(integer sequence, integer track, integer lineFrom, integer lineTo)`        
Copies lineFrom to lineTo. Returns false if cannot get line table from lineFrom, else true. 

`void jt.setGridStep(integer sequence, integer step)`         
Sets grid_step for selected sequence.

### Getters

`integer jt.getNumlin(integer sequence)`    
Returns the number of lines of selected sequence. 

`integer jt.getNumlin(integer sequence)`    
Same as jt.getNumlin.

`integer jt.getNumcol(integer sequence, integer track)`    
Returns the number of columns of selected track, in selected sequence. 

`integer jt.getColnbr(integer sequence, integer track)`    
Same as jt.getNumcol.

`integer jt.getTracknbr()`     
Returns the number of tracks. 

`string jt.getTempoAt(integer sequence, integer line, integer column)`    
Returns tempo value at given indexes, or `nil` if there is no value. Value is a number stored as a string. Use `tonumber` to convert it as a number.  

`string jt.getAt(integer sequence, integer track, integer line, integer column)`    
Returns value at given indexes, or `nil` if there is no value. Values are stored as strings. Use tonumber to convert it to a number if possible. 

`table jt.getLine(integer sequence, integer track, integer line)`    
Returns a table containing line values if it can find selected line. Else returns nil.

### Others

`void jt.clearTrack(integer sequence)`   
Clears sequence

`void jt.clearTrackAtSeq(integer sequence, integer track)`    
Clears track in selected sequence. 

## FFT API     

This API allows you to process FFT data from audio file, and write to audio file. You will receive raw data from FFT, and return your processed raw data to IFFT. Be careful that this API is static, so you can't do two FFT at the same time.

`boolean fft.init(string filefrom, string fileto, integer fft_size)`     
Inits buffers and data for file reading/writing. Return `true` if successful, else `false`. 
`filefrom` and `fileto` must be full path (to an existing soundfile for the first, to a non existing file for the second). 
This method must be called first. `fileto` argument can be `nil` if you don't need an output soundfile (just data processing). 

`boolean, number fft.readBlock()`     
Reads a block of fft_size of input soundfile (block size multiplied by number of channels of input soundfile). Returns success status (boolean) and number of samples read (or nil if not initialized). 

`boolean  fft.writeBlock()`     
Writes the current buffer to the output soundfile. Returns success status.

`integer fft.getChannelCount()`     
Returns the number of channels of input soundfile or nil if not initialized.

`integer fft.getFrameCount()`     
Returns the number of frames of input soundfile, or nil.

`integer fft.getSampleRate()`     
Returns the sample rate of input soundfile, or nil. 

`boolean, table fft.getChannel(integer channel)`     
Copies audio buffer of current channel, and returns it. Returns success status code and the audio data (or nil).

`boolean fft.copyChannel(integer channel)`     
Copies audio buffer of current channel in FFT buffer, and returns success status.

`̀boolean, data fft.FFT(integer channel)`     
Process FFT on the selected channel. Returns success status, and FFT output buffer (or nil).

`boolean fft.IFFT(integer channel)`    
Process IFFT of current FFT buffer, and copies it in samples buffer at channel. Returns success status.

`boolean fft.setData(table data)`    
Sets FFT Buffer after internal Lua processing. This is the buffer that will then be processed by a call to `fft.IFFT`. Returns success status. 

`boolean fft.display(string soundfile, integer fft_length, integer luminosity)`
Returns success status. Displays the soundfile in a FFTPlot in a new dialog. 

## MATH UTILS

Simple math utilities for data processing. 

`mathutils.hann(integer index, integer length)`
Returns hann windowing sample at index. 

`mathutils.hamming(integer index, integer length)`
Returns hamming windowing sample at index. 

`mathutils.blackman(integer index, integer length)`
Returns blackman windowing sample at index. 