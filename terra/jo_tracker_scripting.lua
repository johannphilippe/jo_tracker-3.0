--[[
--LUA SCRIPTING API FOR JO_TRACKER
--	Provides access to track data
--	Need to be extended with GEN data : 
--		- a way to get interpolation values from GEN curves (ugen ?)
--		- a way to know if a value can be written
--	Need to be extended with Orchestra data : 
--		- know number of parameters of an instrument
--		- check if instrument exists
--]]

local json = require"json"
local user_score_data = require"score_data"
local jt = {}

jt.score_data = json.parse(user_score_data)

function jt.setNumlin(seq, numlin) 
	local s = tostring(seq)
	if jt.score_data["lignbr"]==nil then jt.score_data["lignbr"]={} end
	jt.score_data["lignbr"][s]=numlin
end

function jt.setLignbr(seq,numlin)
	jt.setNumlin(seq,numlin)
end

function jt.setNumcol(seq,track,numcol)
	local s = tostring(seq)
	local t = tostring(track)
	if jt.score_data["colnbr"] == nil then jt.score_data["colnbr"]={} end
	if jt.score_data["colnbr"][s] == nil then jt.score_data["colnbr"][s] = {} end
	jt.score_data["colnbr"][s][t]=numcol
end

function jt.setColnbr(seq,track,ncol)
	jt.setNumcol(seq,track,ncol)
end

function jt.setTracknbr(tracknbr)
	jt.score_data["tracknbr"]=tracknbr
end






function jt.getNumlin(seq)
	local s = tostring(seq)
	if jt.score_data["lignbr"] == nil then jt.score_data["lignbr"] = {} end
	if jt.score_data["lignbr"][s] == nil then return nil end
	return jt.score_data["lignbr"][s]
end

function jt.getNumcol(seq,track)
	local s = tostring(seq)
	local t = tostring(track)
	if jt.score_data["colnbr"] == nil then jt.score_data["lignbr"] = {} end
	if jt.score_data["colnbr"][s] == nil then return nil end
	if jt.score_data["colnbr"][s][t] == nil then return nil end
	return jt.score_data["colnbr"][s][t]
end

function jt.getLignbr(seq)
	return jt.getNumlin(seq)

end

function jt.getColnbr(seq,tk)
	return jt.getColnbr(seq,tk)
end

function jt.getTracknbr() 
	if jt.score_data["tracknbr"]==nil then return nil end
	return jt.score_data["tracknbr"]
end





function jt.setTempoAt(seq, lin, col, val)
	local s = tostring(seq)
	local l = tostring(lin)
	local c = tostring(col)

	if jt.score_data["tempo"] == nil then jt.score_data["tempo"] = {} end
	if jt.score_data["tempo"][s] == nil then jt.score_data["tempo"][s] = {} end
	if jt.score_data["tempo"][s][l] == nil then jt.score_data["tempo"][s][l] = {} end
	jt.score_data["tempo"][s][l][c] = tostring(val)
end

function jt.getTempoAt(seq,lin,col)
	local s = tostring(seq)
	local l = tostring(lin)
	local c = tostring(col)

	local result = nil
	if jt.score_data["tempo"] ~= nil and
		jt.score_data["tempo"][s] ~= nil and
		jt.score_data["tempo"][s][l] ~= nil and 
		jt.score_data["tempo"][s][l][c] ~= nil then
		result = jt.score_data["tempo"][s][l][c]
	end
	return result
end

function jt.setAt(seq, tk, lin, col, val)
	if tk > jt.getTracknbr() then jt.setTracknbr(tk) end
	if lin > jt.getNumlin(seq) then jt.setNumlin(seq, lin) end
	if col > jt.getNumcol(seq,tk) then jt.setNumcol(seq,tk, col) end

	local s = tostring(seq)
	local t = tostring(tk)
	local l = tostring(lin)
	local c = tostring(col)

	local value = tostring(val)
	
	if jt.score_data["score"] == nil then jt.score_data["score"] = {} end
	if jt.score_data["score"][s]==nil then jt.score_data["score"][s]={} end
	if jt.score_data["score"][s][t]==nil then jt.score_data["score"][s][t]={} end
	if jt.score_data["score"][s][t][l]==nil then jt.score_data["score"][s][t][l]={} end
	jt.score_data["score"][s][t][l][c]=value
end


function jt.getAt(seq,tk,lin,col)
	local s = tostring(seq)
	local t = tostring(tk)
	local l = tostring(lin)
	local c = tostring(col)

	local result = nil

	if(jt.score_data["score"][s] ~= nil and 
		jt.score_data["score"][s][t] ~= nil and 
		jt.score_data["score"][s][t][l] ~= nil and
		jt.score_data["score"][s][t][l][c] ~= nil) then
		
		result = jt.score_data["score"][s][t][l][c]
	end
	return result
end



-- fills column in seq with value
function jt.setColumn(seq, tk, col, val)
	local s = tostring(seq)
	local t = tostring(tk)

	if jt.score_data["lignbr"][s] == nil then return nil end
	local lignbr = jt.score_data["lignbr"][s]
	
	if jt.score_data["colnbr"][s][t] == nil then return nil end
	local colnbr = jt.score_data["colnbr"][s][t]
	
	for i = 1, lignbr do
		if( jt.getAt(seq,tk,i,1) ~= nil ) then
			jt.setAt( seq, tk, i, col, val )
		end
	end
end

-- sets a line with vakues contained in a table
function jt.setLine(seq,tk,line,t_val) 
	if type(t_val)~="table" then 
		return false
	end

	for k,v in pairs(t_val) do
		jt.setAt(seq,tk,line,k,v)
	end
	return true
end


function jt.copyLineTo(seq,tk,line, lineto)
	local line_tab = jt.getLine(seq,tk,line)
	if line_tab ~= nil then 
		jt.setLine(seq,tk,lineto, line_tab)
		return true
	else 
		return false
	end
end


function jt.getLine(seq,tk,line)
	local result = nil

	if(jt.tracker_data[s] ~= nil and 
		jt.score_data[s][t] ~= nil and 
		jt.score_data[s][t][l] ~= nil and
		js.score_data[s][t][l][c] ~= nil) then
		
		local number = jt.getColnbr(seq,tk)
		if number ~= nil then 
			result = {}
			for i = 1, number do
				result[i] = jt.getAt(seq,tk,line,i)	
			end
		end

	end
	return result
end


function jt.setGridStep(seq, step)
	local s = tostring(seq)
	local step_nbr = tonumber(step)

	if jt.score_data["grid_step"] == nil then jt.score_data["grid_step"] = {} end

	jt.score_data["grid_step"][s] = step_nbr
end


function jt.clearTrackAtSeq(seq,track)
	local s = tostring(seq)
	local t = tostring(track)
	jt.score_data["score"][s][t] = nil
end

function jt.clearSeq(seq)
	local s = tostring(seq)

	for i=0, jt.getTracknbr() - 1 do
		jt.clearTrackAtSeq(seq, i)
	end
end



-- new Methods 
--

return jt

