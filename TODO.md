# TODO

- Complete Lua API - access to GEN data, and instrument list 
- Add ExportParameterDialog before offline rendering ?
- Macro/Global definition View
- Handwrite GEN function (geneditortab) - allowing to access every GEN in f statements
- Channels issue : nchnls_i to define in exportparametersdialog (separate from nchnls ?)
- Channels issue : make a difference between number of output channels and number of recorded channels. 
- Routing matrix  - how software channels are directed to hardware output : big question since it influences how rendering is done.
- Important : for heavy tasks (terra processing can be one, loading project too etc...) perform async, with or without a progressbar or progressdialog. 
- Very important : a way to bufferize multiple lines for copy/paste.
- Feature to modify space between events (to expand space / lines btw events)

## Bugs

- IMPORTANT (VERY) : displayInstrumentList not displaying all the time
- IMPORTANT : Sometimes a visual bug when tracks/trackset expands
- GRIDSTEP : not refreshing properly when lignbr changed
- When scrolling track 1 to bottom, other seqs follow to linemax - 1.	(my screen issue, since on other screen it's ok)
- Lua scripter dialog console - everything printed in one line
- New track : doesn't add line number (not even when filling with data). Only does it when removing data. 
- (related to previous) Problem with instr informations and line number not displaying automatically 
- Always plays up to seqmax +1 (or more) 
- AutoCalculateDurations seems very long when recalculate all track - something to do ? Async version 

## Fixed :

- When creating a new seq, find last value of tempo (current tempo) and put it in tempo line 1 col 1. ?
- When play not from start, values in next sequences are fake (p2 p3). - SEEMS TO BE FIXED (not sure)
- Tempo instrument : linear segment of progression/regression (for high tempo refresh) - DONE 
- Option to import every soundfile in project dir when save project - DONE
- When play not from start, find last tempo before start point (current) and add it to t 0. -FIXED
- CleanData makes data disappear - FIXED
- When "write to seq" in sequence arranger, erases grid_step - FIXED
- Lua scripter help button - useless ? Just put a link to markdown doc and open in a webbrowser.

## Future feature list 

- Measure tools : measure time between lines, events visualisation dialog, improve transport with minuts:seconds visualisation,  
- Add Mixer view with number of channels (separate dialog)
- Add console view (bottom of mixer view ?)
- Add Dialog to import / select import / udos and instruments from another project
- Dummy track dialog for testing score and copying.


# INSTALLER : 

- Windows : find a way to automatically install vcruntime140, and fix csound path
