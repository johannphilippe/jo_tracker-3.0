# jo_tracker 3.0.1
#### Beta release of jo_tracker. 

Jo_tracker is a soundtracker software using [Csound](https://csound.com/) as its audio engine. It has been thought as a laboratory tool to experiment and create music. 
In order to use this software properly, some Csound knowledges are required. jo_tracker uses [MIUP](https://framagit.org/johannphilippe/miupcpp) - a homemade C++ library based on [IUP - Portable User Interface](https://www.tecgraf.puc-rio.br/iup/) - for graphical user interface. 
It is available on Linux - tested on Mint 19.1 and 20 - Cinnamon, and Windows 10. A release for MacOSX is not part of plans
now, as it would require a lot of work. It might be done in the future, though it might require contributors and testers. If you are interested in, just write to johannphilippe@lilo.org.  

# Version

It is called version 3.0, though it is the first public release. This is because I didn't want to erase the history of its creation. The first prototype of jo_tracker was a Max patch, using `csound~` as its engine. Of course, when the project became more complex, it became full of bugs. It has then been rewrote in Lua, using Terra interpretor, and Csound API. It became more stable. But, the Lua codebase was not easy to maintain. And some of the new functionnalities of the tracker quickly required a better architecture, and a slightly lower level of programming. This is why it was rewrote, for the second time, in C++, starting in decembre 2018. 

## Materials

This work has been presented at the International Csound Conference 2019 in Cagli (Italy). Links to the paper and the video of the conference are below.  

- [Paper](https://csound.com/icsc2019/proceedings/4.pdf)
- [Video](https://www.youtube.com/watch?v=8ldQsjzYBNk)

## Builds 

- Linux : Built on Linux Mint 20 Cinnamon, with clang version 10.0.0-4ubuntu1, Target: x86_64-pc-linux-gnu, Thread model: posix
- Windows 10 : Built with visual studio

## Summary 


- [Installation](#install)
- [Csound](#csound)
- [Get Started](#getstarted)
- [Menus](#menus)
- [Main tab - Tracks](#tracks)
- [Terra](#terra)
- [Second Tab - Gen Editor](#secondtab)
- [Third tab - Csound code editor](#thirdtab)
- [Output modes](#output)
- [Good practices](#practices)
- [Known Issues](#issues)
- [Features to come](#features)


## <a name="install">Installation</a>
To install jo_tracker, you can run install.rb script as an administrator or root, and it will install all dependencies for you. 
On Linux, simply run ```sudo ruby install.rb``` in the terminal.
On Windows, you can launch a terminal (cmd, powershell, or any terminal) as an administrator and run ```ruby install.rb```
You will need ruby interpretor in order to do this. 

## <a name="csound">Csound</a>  
[Csound](https://csound.com/) is "a sound and music computing system". It can be seen as a programming language containing abstractions and facilities for audio computing. If you are new to Csound, you might want [to get started](https://csound.com/get-started.html) with it.

## <a name="getstarted">Get Started</a>
jo_tracker is inspired by soundtrackers, and acts as a step sequencer with vertical time representation (from top to bottom). Its tracks are designed like spreadsheets. Though, since jo_tracker's aim is to benefit of Csound synthesis power, it doesn't work exactly like most soundtrackers. When you first open the progam, the main dialog opens. At the top, it contains menus to manage project files, audio renderers, and audio parameters. Below menus, the transport bar contains a gain slider, two gain meters, and two realtime scopes. Below this, you will see three tabs - it represents main tools of jo_tracker. 

### <a name="menus">Menus</a>
Top menu bar provides some file management facilities, and audio parameters management. 
The Files menu contains following items : 

- New Project : clears any data, and starts a new project from scratch. It will erase all non-saved data.
- Save Project : Saves project data as a ".jo" file (actually in json format). 
- Gather and Save Project : saves project data in the same way, but also copies all soundfile (GEN 01 definitions in Gen Editor Tab) in the selected directory. 
- Load Project : simply loads a project from a ".jo" file. It will also erase all non-saved data and perform a path resolution for Gen 01 data - in the case project has been saved with "Gather and Save Project".
- Export as ".csd" file : It will generate and write a .csd file to the selected location. If you choose the "gather soundfiles" option, it will also copy GEN 01 sounfiles, and replace filepath in GEN 01 definition by relative path (filename only). 
- Render - offline rendering (WAV soundfile).

Parameters menu contains audio parameters. It will open a dialog allowing to define : 

- Number of channels
- Sample rate
- Ksmps
- 0dbfs
- Additional options in Csound options format. 

It also contains a dialog allowing to modify the size and spacing of tracks. 

### <a name="tracks">Main tab - Tracks</a>
The tracks view is used to manage score instructions in a soundtracker fashion. Which means this is where you describe the actual events that will happen over time when performance is on.   
At the top of this view, there are four spinboxes : 

- SEQNBR : is used to select the current sequence and recall a sequence. Sequences are blocks of time acting like sections in Csound - the second one will start to play only once the first has finished. 
- TRACKNBR : The number of tracks. This number is "global", since it is the same for the whole project.
- LIGNBR : The number of lines for current sequence. Each line acts as a step. You can set any number of lines you like for each sequence. 
- GRID STEP : Changes the color of lines every X steps. It can be useful if you want to write with a particular time signature. Its value can be different for each sequence. 

Below those spinboxes, the first track, called jtrack0. It contains a set of buttons at the top. 

- First button clears the track (only for current sequence). 
- Second one expands the track so you can see every parameters it contains. 
- Third button is actually a spinbox, used to define the number of columns (so the number of p-fields parameters) of the current track (only for current sequence, since a track can have different number of columns in each sequence). 
- Fourth button is used to manage track parameters. For now, only autocalculation method that can be set here. This will be explained below. 
- Fifth button is designed to delete the current track. It is not implemented yet (button does nothing). 

Any track cannot contain less than 4 columns which will be mapped to Csound first 3 p-fields of an i statement. First two columns are used to write the instrument name, and its instance number (optional). It is important to remember that instrument name won't be kept in generated Csound score. Jo_tracker will convert any used instrument name into an instrument number, and append its instance number as its decimal part. Third column is used to delay an event from its step. For example, a value of 0.5 will delay the event of half a step. Be careful, every delay value will actually work (even negative one), so you can even delay an event of 4 step (which can cause hedeaches to read later). Fourth parameter is duration of event(p3 in Csound). It can be automatically calculated or not. There are three modes you can choose in the track parameters for auto-calculation of duration : 

- Instance : The note lasts until the next occurrence of same instrument with same instance number. Two events are considered as being of the same instance if they share the same instrument name AND the same instance number. If no instance number is set, then only instrument name is taken to determine it. If there is no next event of same instance, you can set duration yourself. 
- Occurrence : The note lasts until there is any next event in the same track. If there is no next event, you can set duration yourself. 
- None : You have to set duration yourself. 

For any of those modes, you can still handwrite duration if (and only if) it is shorter thant what auto-calculate mode would actually calculate. This can cause a problem when you move an event located
between two events. Backspace on any event duration cell will cause events duration to be re-calculated.
This parameter can be set to a different mode for each track. But, the mode will act for all sequences in this particular track. 
Columns after fourth are optional. All cells are stored as strings, so you should be able to write anything you like in those optional columns (including Csound particular carry instructions and more). You should know that data you write will be directly translated to Csound "i" statement.

Main tab also contains a tempo track. It is used to set tempo at particular times. It is mapped to Csound "t" statement. Left column is used to set tempo at startup (first sequence), and to gliss between two tempo values. For example, write 60 in the first column/first line, and 120 in first column/fifth line, and your tempo will move from 60 to 120 in five steps. To make a direct jump to a tempo to another, you have to write on the left the value from where it jumps, and on the right the value to jump to. So you can combine a gliss and a jump, by just adding 240 in second column/fifth line in previous example. This tempo track can be hidden with arrow expand button on the right. 

There is another view that is hidden when launching jo_tracker : the arrow on the left will expand a small view on the left of the dialog. This is an experimental tool that can be used to define sequences order. For exemple let's say you wrote three sequences, but you now want to play the third before the second one. This view can be used for this. The spinbox at the top of the view can be used to add or remove an item to the new dataset you're defining. For each line, the left spinbox is used to identify the sequence index in old data set, and the right spinbox is used to specify a number of loops. So you can even repeat three times the second sequence, before play the first one once, and the third one twice, and again the second three times. To process those changes, there are two buttons : Play, and Write as Seq. The Play button will perform with those changes, without causing any changes to user data. "Write as seq" will apply the new sequences order to user data. Be careful, you won't be able to go back. In the previous example, sequences 1,2, and 3 will become what we called sequence 3 before changes etc... 

Last but not least - you might have seen that main tab contains a small button on the right with Lua logo. The tracker embedded a Lua interprettor : [Terra](http://terralang.org/) and an API to generate data. Due to incompatibilities with Terra new builds, the Linux build now links to luajit.

#### <a name="terra">Terra interpretor</a>
[Terra](http://terralang.org/) is a low-level system programming language that is embedded in and meta-programmed by the Lua programming language. Which means it can run any valid Lua code. But it can do more. It provides a particular syntax and types used for low-level computing. While Lua code is interpreted on the fly, Terra functions are compiled (still at runtime), and allow to use C libraries. 
Jo_tracker's implementation of Terra is mostly used for scripting in Lua the content of tracks. Clicking on the Lua button opens a Lua scripter dialog. This interpretor has an access the data that already are inside your tracks (even tempo track), and can also write data inside tracks. As a new feature, it is experimental, and needs to be battle tested. So it can be an inspiring tool to generate some music from deep and abstract algorithmic thoughts !

### <a name="secondtab">Second tab - GEN editor</a>
This tab contains itself four subtabs. Each one is used to manage Csound's GEN functions, in order to use it in your Csound instruments. The spinbox at the top is used to define and recall a GEN with its index. Those indexes will be directly mapped to Csound "f" statement indexes. Let's describe those four tabs : 

- Curve editor : Allows you to write linear and curved functions. It contains itself three mode (you can change with radio buttons at the bottom). To write functions, simply click on the canvas and move your points. A double click on a point will snap it to the grid. A shift+click on a point will delete the point. The Curve mode is used to write some linear and/or logarithmic/exponential curves. It will be mapped to Csound's GEN 16. To transform a segment to a curve, simply do shift+click on the segment, and while click is not released, move up/down your mouse cursor. The second mode writes cubic spline curves as Csound's GEN 08. Unlike first mode, it needs to contain at least three points. Third mode allows you to write quadratic bezier's curves as Csound's GEN quadbezier. It also needs at least three points, but also requires an odd number of points. Each odd indexed points are the points the curve actually passes by. Even indexed points are control points.  
- Waveform uses a still experimental waveform visualisation tool. Load button allows you to import a soundfile that will be drawn on the screen. It will be mapped as Csound's GEN 01, allowing you to use sound samples in your Csound instruments. 
- Matrix based Gen tab is used to write directly two kind of functions : simple numeric arrays (mapped to GEN 02), or an partial description of wavetables (GEN 09). In this second case, the first column represents the harmonic number (can be decimal), second column is it's amplitude, and third column is it's initial phase. 
When you click on store button, it will then store you GEN data in the current project, at the given index (Function number). Be careful that, if another function was already stored at this index, it will be replaced by new one. Other buttons don't really need to be introduced : clear button clears (but do not removes from data), remove button removes. 
- Scriptable Curve : Composed of a plot and a code editor (Lua), it allows you to script a curve. The Execute button will show the result of your script. But you still need to click on Store button to save your curve. When you store, the script is saved, allowing you to see you curve when you need. And the data itself (the points of the curve) are saved to a CSV internal file, that will be imported in Csound as a GEN 23. 

To use a GEN function inside score, simply write its index in any track cell. 
Curve functions (curve, spline and bezier) are by default scaled from 0 to 1. But you might want another scale (to use as a frequency parameter for example). In the track, you can also write a scale macro with this syntax : `s idx,scalemin,scalemax`. This syntax can be used as well when writing functions in GEN 02 matrix. For example if you want to scale function at index 1 for frequency, you can write : `s1,500,800`. This will cause jo_tracker to write the exact same function with the new scale, and pass it to Csound when performance starts. The cell content `s,1,500,800` will be internally replaced by the negative index that jo_tracker gives to this scaled function when starting performance or rendering. It automatically attributes an index, given the number of functions you already stored. For example, if you stored 5 functions in your current project, and your tracks asks jo_tracker to generate 3 different scales, those three functions will receive indexes 6, 7, and 8. But the "i" statement corresponding to the cell asking to scale will receive number -6, -7 or -8. So make sure that you instrument takes the absolute value of this parameter. The reason why it is passed as a negative index is to avoid conflict with an actual value you would be passing as a Csound p-field. For example, if a p-field is dedicated to frequency, you can assume that any positive value will be a fixed frequency value, and that any negative value will be a frequency function that will need to be read with a `table` opcode. 

### <a name="thirdtab">Third tab - Csound code editor</a>
New feature in version 3.0 - jo_tracker now embbed a code editor for csound, allowing you to write instruments and UDO's directly in the software. It provides a syntax highlighting for Csound keywords and opcodes, but also for UDO's. 

- Instruments : When user writes an instrument, he must fill the instrument name field (first text box, just after "instr"). He can also write a comma separated list of p-field description (that will be visible in the first line of the track, when writing a score instruction with this instrument). Then, user must write its instrument in the code editor. It is the exact same syntax as Csound, except that the first and last line of instrument definition ("instr 1" and "endin") mustn't be specified. Once instrument body is written, user can click "Add/Update" button. It will cause a Csound evaluation of automatically formatted code. This evaluation will contain every defined UDO required by current instrument, in a recursive way (every UDO required by required UDO ...). If evaluation is successful, it will be inserted in instrument list. Otherwise, it will return an error to the console. Be careful that error line might not be really helping here, because the real evaluation code prepends required UDO's and some global variables. Instrument name is only used in jo_tracker. When performance or render is triggered, jo_tracker attributes a number to any used instrument, and replaces its name in score statement by this number. Instruments audio output shouldn't use "out", "outch", "outs" opcodes directly. According to the number of channels defined in audio parameters (Parameter menu), jo_tracker will generate a global audio array called "gaOut", with the right number of channels. To output some audio in any channel, user has to write something like this : `gaOut[channel_nbr + 1] = gaOut[channel_nbr + 1] + asig`. The "+ 1" is because arrays are indexed from 0, so if you want to output to first channel, you should add you signal to gaOut[0]. An already defined UDO is defined `setCh(channel_nbr, asig)`. Its aim is to write with the write channel index. This global audio array is written to output channels and cleared in a automatically formatted master instrument. 
- UDO's (user defined opcodes) : Requires user to write opcode name, as well as input and output types. Then, user can write opcode body in the code editor. Same as for instruments, it will make an evaluation with required UDO's.  

This editor also provides Csound manual. When editor is in focus mode, any press on "Ctrl + h" shortcut will show a dialog. If the current word (word in editing) is a csound known opcode, this dialog will display its help. Otherwise, it will display the manual homepage. 

## <a name="output">Output modes</a>
Output modes refer to how those data are finally given to Csound. It contains realtime and non-realtime processing of automatically generated ".csd" file. In any of those modes, jo_tracker will automatically create a .csd file, containing : 

- Options : `--output=dac` for realtime modes or .csd export, and `--output=filename.wav` for non-realtime rendering. 
- Globals : sr, ksmps, nchnls and 0dbfs according to parameters (Parameter menu), and number of audio globals you set in nchnls parameter. 
- Metro instrument : called instrument 1 - it is used internally by jo_tracker to display on tracks the line that is currently played. It also propagates a tempo related value through a `chnset` opcode, so that user can get tempo from any instrument. This value sent throught this channel is equal to (1 / (tempo / 60) ). For example, if tempo is set to 120, this value will be 0.5. With a tempo of 60, it will be 1. 
- Master instrument : called instrument 999. It is formatted in a different way whether it is in realtime or not. In any case, it outputs audio global to corresponding DAC output(ga1 to output 1, ga2 to 2...). In realtime mode, it also gets the value of master gain slider (transport bar on the top), and multiply outputs with it's value. It also sends a the current gain level of output channels to gain meters and scopes of transport bar (currently limited to two, but it might change in the future with a mixer dialog).

There are currently several audio output modes : 
- Realtime play : can be triggered with "Resume Play" button (plays from start) of "Play" button (plays from current sequence/line).
- Realtime recorder : triggered with "Record" button. Useful when your instruments uses input channels or realtime informations. It will record your project in realtime in a .wav file. 
- Offline rendering : triggered in menu "Files - Render". It will ask you for destination file, and render audio as a .wav file. 
- CSD rendering : it will render a .csd file that can be read independantly from jo_tracker. 



## <a name="practices">Good practices</a>
It is recommanded to always start writing instruments first - since tracks first column (instrument name) provides auto-completion for already defined instruments. The track system will also show parameters name in the first line (line 0) when writing score parameters. Last but not least, auto-calculation of duration will only work if instrument has already been defined. 

For making jo_tracker work with a DAW, I recommand to use [jack](https://jackaudio.org/) to manage input/output from DAW to Csound, and from Csound to DAW. This allows to imagine some really complex audio routing, which is fun. 

You will find project examples in this directory. The installer script puts them in your local directory : 

- On Linux : `/home/.local/share/jo_tracker/examples`
- On Windows : `C:/Users/MyUsername/AppData/Local/jo_tracker`

## <a name="issues">Known issues</a>

- When editing curves in Spline mode, adding an important number of points can hide curve representation or even crash jo_tracker. It is due to the spline interpolation library used.
- Sometimes, the small dropdown popup displaying instrument names in main tab doesn't show, and makes the tracker crash. 

## <a name="features">Features to come</a>

- Console console displaying runtime Csound informations
- Write/adapt a UGens Csound API for Terra (it can be in C), so that one can use csound Opcodes inside Terra/Lua code to generate data. 
- More complete Terra API
- Design : logo
- Menus : about jo_tracker menu (version, website...).
