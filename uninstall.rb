#!/usr/bin/ruby


#    Copyright (C) 2019-2020 Johann Philippe

#    This file is part of jo_tracker

#    The jo_tracker software is free software; you can redistribute it
#    and/or modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.

#    jo_tracker is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public
#    License along with jo_tracker; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
#    02110-1301 USA

#    As a special exception, if other files instantiate templates or
#    use macros or inline functions from this file, this file does not
#    by itself cause the resulting executable or library to be covered
#    by the GNU Lesser General Public License. This exception does not
#    however invalidate any other reasons why the library or executable
#    file might be covered by the GNU Lesser General Public License.

puts %x(gem install bundler)

require "bundler/inline"
require "rubygems/package"

puts "Downloading Gem's dependancies"

gemfile do
	source 'https://rubygems.org'
	gem "fileutils", require: true
end
	puts "Gem downloaded"



def uninstall_linux
    puts "On linux, uninstall won't remove libraries used by jo_tracker"

    FileUtils.rm "/usr/bin/jo_tracker"
    FileUtils.rm "/usr/share/applications/jo_tracker.desktop"
    FileUtils.rm_r "/usr/share/icons/jo_tracker"

    puts %x(sudo update-desktop-database /usr/share/applications)
    puts "Do you want to remove internal non-saved data of jo_tracker [y, n]"
    val = gets
    val = gets.downcase.strip
    if(val == "yes") then
	    jo_tracker_path = Dir.home + "/.local/share/jo_tracker"
        FileUtils.rm_r jo_tracker_path
    end
    
    puts "jo_tracker uninstalled successfully"
end



def uninstall_windows
    FileUtils.rm_r "C:/Program Files/jo_tracker"

    puts "Do you want to remove internal non-saved data of jo_tracker [y, n]"
    val = gets
    val = gets.downcase.strip
    if(val == "yes") then
        jo_tracker_path = Dir.home + "/AppData/Local/jo_tracker"
        FileUtils.rm_r jo_tracker_path
    end
    puts "jo_tracker uninstalled successfully"
    
end



print "Uninstall jo_tracker, are you sure ? [y,n] : "
v = gets
val = v.downcase.strip	



if val == "y" or val =="yes" 
	if (RUBY_PLATFORM =~ /win32/) or (RUBY_PLATFORM =~ /mingw/)
		puts "jo_tracker for Windows"
		uninstall_windows
	elsif RUBY_PLATFORM =~ /linux/
		puts "jo_tracker for Linux"
		uninstall_linux
	else
		puts "jo_tracker is not available on this platform."
	end
else
	puts "Uninstallation canceled"
end
